import { getProjects } from "$lib/server/projects.js"

export const config = {
    isr: {
        expiration: 3600
    }
}

export const load = async () => {
    return {
        presentation: {
            name: "Víctor González",
            bio: "Full-stack Developer",
            gitlab: "https://gitlab.com/mrvik",
            telegram: "https://t.me/victorgzfdz",
            linkedin: "https://www.linkedin.com/in/v%C3%ADctor-gonz%C3%A1lez-720448a8/"
        },
        projects: await getProjects().then(formalize)
    }
}

const formalize = ({ data, error }) => {
    if (error) {
        console.error(error)

        return {
            error: error?.message ?? "Internal error"
        }
    }

    const userStarred = new Set(data.user.starredProjects.nodes.map(p => p.id))

    const list = data.user.projectMemberships.nodes
        .map(n => n.project)
        .filter(({ id }) => userStarred.has(id))

    return { list }
}
