import { gql, Client, cacheExchange, fetchExchange } from "@urql/core"

import { env } from "$env/dynamic/private"

const { GITLAB_ACCESS_TOKEN } = env

const projectsQuery = gql`
    query UserProjects {
        user(username: "mrvik") {
            id
            starredProjects {
                nodes {
                    id
                }
            }
            projectMemberships {
                nodes {
                    project {
                        id
                        name
                        description
                        fullPath
                        link: webUrl
                        forks: forksCount
                        languages {
                            name
                            share
                        }
                        stars: starCount
                    }
                }
            }
        }
    }
`

const client = new Client({
    url: "https://gitlab.com/api/graphql",
    exchanges: [cacheExchange, fetchExchange],
    fetchOptions: () => ({
        headers: {
            Authorization: `Bearer ${GITLAB_ACCESS_TOKEN}`
        }
    })
})

export const getProjects = () => client.query(projectsQuery).toPromise()
